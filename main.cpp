
#include <iostream>
#include <string>
#include <string_view>
#include <filesystem>
#include <fstream>
#include <cstdlib>

#include <numeral-system/numeralSystem.hpp>
#include <options-parsing/optionsParser.hpp>
#include <utf-algorithms/algorithms.hpp>
#include <utf-algorithms/converter.hpp>

// #define BWN_UTF8_CONSOLE

std::u32string getInputTill(std::u32string_view p_stopLine, bool useUtf8Input = false)
{
	std::u32string ret;

	using Utf8Converter = utf::Converter<char32_t, utf::Char8>;
	using WcharConverter = utf::Converter<char32_t, wchar_t>;
	while (true)
	{
		std::u32string normilizedInput;
		if (useUtf8Input)
		{
			std::string input;
			std::getline(std::cin, input);


			normilizedInput = Utf8Converter::convertToContainer<std::u32string>(input);
		}
		else
		{
			std::wstring input;
			std::getline(std::wcin, input);

			normilizedInput = WcharConverter::convertToContainer<std::u32string>(input);
		}

		if (normilizedInput == p_stopLine) {
			return ret;
		}
		
		ret += normilizedInput;
		ret.push_back(U'\n');
	}
}

static const std::u32string_view russian_alph_low{ U"_абвгдеёжзийклмнопрстуфхцчшщъыьэюя" };
static const std::u32string_view russian_alph_high{ U"_АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" };
static const std::u32string_view english_alph_low{ U"_abcdefghijklmnopqrstuvwxyz" };
static const std::u32string_view english_alph_high{ U"_ABCDEFGHIJKLMNOPQRSTUVWXYZ" };

size_t findInTable(std::u32string_view p_table, const uint32_t p_letter)
{
	for (size_t letterId = 0; letterId < p_table.size(); ++letterId )
	{
		if (p_table[letterId] == p_letter)
		{
			return letterId;
		}
	}

	return std::u32string_view::npos;
}

bwn::NumeralSystem toNumeral(
	const char32_t*& p_beg,
	const char32_t*const p_end,
	const std::vector<std::u32string_view>& p_tables)
{
	if (p_tables.empty()) {
		throw std::runtime_error{ "No tables were provided." };
	}

	const size_t tableSize = p_tables[0].size();

	for (const std::u32string_view& table : p_tables)
	{
		if (table.size() != tableSize) {
			throw std::runtime_error{ "Incorrect tables with different sizes." };
		}
	}

	if (tableSize < 2) {
		throw std::runtime_error{ "Table size less then 2." };
	}

	bwn::NumeralSystem ret{ static_cast<bwn::NumeralSystem::Digit>(tableSize) };

	while (p_beg < p_end)
	{
		std::size_t pos = std::u32string_view::npos;

		for (auto tableIt = p_tables.begin(); tableIt != p_tables.end() && pos == std::u32string_view::npos; ++tableIt) {
			pos = findInTable(*tableIt, *p_beg);
		}

		if (pos == std::u32string_view::npos) {
			return ret;
		}

		ret.PushDigit(static_cast<bwn::NumeralSystem::Digit>(pos));
		++p_beg;
	}

	return ret;
}

std::u32string fromNumeral(
	const bwn::NumeralSystem& p_number,
	std::u32string_view p_table)
{
	if (p_table.size() != static_cast<size_t>(p_number.GetBase())) 
	{
		throw std::runtime_error{ "Table size not equal to number base." };
	}

	std::u32string ret;
	ret.reserve(p_number.GetLength());

	for (std::size_t digitId = 0; digitId < p_number.GetLength(); ++digitId)
	{
		const size_t index = static_cast<size_t>(p_number[digitId]);
		if (index >= p_table.size())
		{
			throw std::runtime_error{ "Incorrect digit in the table." };
		}

		ret.push_back(p_table[index]);
	}

	return ret;
}

std::u32string encode(
	std::u32string_view p_input,
	const std::vector<std::u32string_view>& p_encodingTables,
	std::u32string_view p_decodingTable)
{
	std::u32string output;
	output.reserve(p_input.size());

	const char32_t* inputBeg = p_input.data();
	const char32_t*const inputEnd = inputBeg + p_input.size(); 

	while (inputBeg != inputEnd)
	{
		bwn::NumeralSystem number = toNumeral(inputBeg, inputEnd, p_encodingTables);

		if (number)
		{
			number.ChangeBase(p_decodingTable.size());

			output += fromNumeral(number, p_decodingTable);
		}

		if (inputBeg != inputEnd) {
			output.push_back(*inputBeg);
		}

		++inputBeg;
	}

	return output;
}

void readEncodingTables(
	const std::filesystem::path& p_tablesFile,
	std::vector<std::u32string>& o_encodingTables,
	std::vector<std::u32string>& o_decodingTables)
{
	using StringIter = std::string::const_iterator;
	const std::string input = [&p_tablesFile]() -> std::string
	{
		std::ifstream file(p_tablesFile, std::ios::in);

		if (!file) {
			throw std::runtime_error{ "File \"" +  p_tablesFile.string() + "\" can't be opened." };
		}

		std::string data;
		file.seekg(0, std::ios::end);
		data.reserve(file.tellg());
		file.seekg(0, std::ios::beg);

		data.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

		return data;
	}();

	o_encodingTables.clear();
	o_decodingTables.clear();

	const auto tryGetNumber = [](StringIter p_beg, StringIter p_end, uint32_t& o_ret) -> bool
	{
		while (p_beg != p_end && std::isspace(*p_beg)) ++p_beg;

		uint32_t ret = 0;
		bool atLeastOneDigit = false;
		while (p_beg != p_end && *p_beg >= '0' && *p_beg <= '9')
		{
			atLeastOneDigit = true;
			ret *= 10;
			ret += *p_beg - '0';
			++p_beg;
		}

		while (p_beg != p_end && std::isspace(*p_beg)) ++p_beg;

		o_ret = ret;
		return p_beg == p_end && atLeastOneDigit;
	};

	const auto findEndLine = [](StringIter p_beg, StringIter p_end) -> StringIter
	{
		while (p_beg != p_end && *p_beg != '\n') ++p_beg;

		return p_beg;
	};

	const auto readTable = [&findEndLine, &tryGetNumber](StringIter& p_beg, StringIter p_end) -> std::u32string
	{
		std::u32string ret{};
		
		while (p_beg != p_end)
		{
			StringIter tempEnd = findEndLine(p_beg, p_end);

			uint32_t number;
			if (!tryGetNumber(p_beg, tempEnd, number))
			{
				break;
			}

			ret.push_back(number);
			
			p_beg = tempEnd + (tempEnd != p_end); // no need to add 1 if this is the end of a string
		}

		return ret;
	};

	const auto findTableStart = [&findEndLine](StringIter p_beg, StringIter p_end, bool& o_isEncoding) -> StringIter
	{
		const std::string_view tempView(&*p_beg, p_end - p_beg);
		const size_t encodingTableIndex = tempView.find("encoding:");
		const size_t decodingTableIndex = tempView.find("decoding:");

		if (encodingTableIndex == std::string_view::npos
			&& decodingTableIndex == std::string_view::npos)
		{
			return p_end;
		}

		const StringIter tableHeaderIt = p_beg + std::min(encodingTableIndex, decodingTableIndex);
		const StringIter newLineIt = findEndLine(tableHeaderIt, p_end);

		o_isEncoding = encodingTableIndex < decodingTableIndex;

		return newLineIt + (newLineIt != p_end);
	};

	StringIter beg = input.begin();
	StringIter end = input.end();

	while (beg != end)
	{
		bool isEncoding = false;
		StringIter tableStartIt = findTableStart(beg, end, isEncoding);

		if (tableStartIt == end)
		{
			break;
		}

		std::u32string table = readTable(tableStartIt, end);

		if (!table.empty())
		{
			(isEncoding ? o_encodingTables : o_decodingTables).push_back(std::move(table));
		}

		// At this point table start iterator will point to the end of the table,
		// because of readTable method advancing iterator.
		beg = tableStartIt;
	}
}

int main(int argc, char* argv[])
{

	struct
	{
		bool printHelp = false;
		bool decode = false;
		bool utf8Console = false;
		const char* tablesPath = nullptr;
	} options;
	
	{
		bwn::OptionsParser parser;
		parser.registerOption("--help", options.printHelp);
		parser.registerOption("--decode", options.decode);
		parser.registerOption("--utf8-console", options.utf8Console);
		parser.registerVaraible("--tables", options.tablesPath);

		parser.parse(argc, argv);

		if (!parser.getFailedArgs().empty())
		{
			std::cerr << "[BWN][ERROR] Failed to parse arguments: ";
			bool first = true;
			for (const char*const arg : parser.getFailedArgs())
			{
				if (!first)
				{
					std::cerr << ", ";
				}
				std::cerr << arg;
			}
			std::cerr << std::endl;
			return -1;
		}

		if (options.printHelp)
		{
			std::cout 
				<< "Simple text encoder.\n" << parser.formatHelp() 
				<< "\nUsage:\nPrint text to encode. To stop, print ':q' on single line." << std::endl;
			return 0;
		}
	}
	// options.decode = true;
	// options.utf8Console = true;
	// options.tablesPath = "tables.txt";
	try
	{
		std::vector<std::u32string_view> encodingTables;
		std::vector<std::u32string_view> decodingTables;
		std::vector<std::u32string> __encodingTables;
		std::vector<std::u32string> __decodingTables;

		if (options.tablesPath != nullptr)
		{
			std::cout << "[BWN] Using encoding tables from file: " << options.tablesPath << '\n';

			readEncodingTables(options.tablesPath, __encodingTables, __decodingTables);
			
			encodingTables.reserve(__encodingTables.size());
			decodingTables.reserve(__decodingTables.size());

			for (const auto& table : __encodingTables) {
				encodingTables.push_back(table);
			}
			for (const auto& table : __decodingTables) {
				decodingTables.push_back(table);
			}
		
		}
		if (encodingTables.empty())
		{
			std::cout << "[BWN] Using default (rus) encoding tables.\n";
			encodingTables.push_back(russian_alph_low);
			encodingTables.push_back(russian_alph_high);
		}
		if (decodingTables.empty())
		{
			std::cout << "[BWN] Using default (eng) decoding tables.\n";
			decodingTables.push_back(english_alph_low);
			decodingTables.push_back(english_alph_high);
		}

		std::cout << "[BWN] As console utf used " << (options.utf8Console ? "utf8" : "wchar") << ".\n";
		std::cout << "Input text (to stop write \":q\"):\n";

		const std::u32string input = getInputTill(U":q", options.utf8Console);
		const std::u32string output = encode(
			input,
			(options.decode ? decodingTables : encodingTables),
			(options.decode ? encodingTables.front() : decodingTables.front())
		);
		
		std::cout << "\n-------------------------------------\n";
		if (options.utf8Console)
		{
			std::cout << utf::Converter<utf::Char8, char32_t>::convertToContainer<std::string>(output);
		}
		else
		{
			std::wcout << utf::Converter<wchar_t, char32_t>::convertToContainer<std::wstring>(output);
		}
		std::cout << '\n';
	}
	catch (const std::exception& e)
	{
		std::cerr << "[BWN][ERROR] " << e.what() << std::endl;
		return -1;
	}

	std::cout << std::flush;
	return 0;
}
